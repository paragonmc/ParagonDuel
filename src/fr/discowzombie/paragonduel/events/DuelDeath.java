package fr.discowzombie.paragonduel.events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import fr.discowzombie.paragonduel.DuelManager;

public class DuelDeath implements Listener {
	
	@EventHandler
	public void onDeath(EntityDeathEvent e){
		if(e.getEntity() instanceof Player){
			Player death = (Player) e.getEntity();
			Player winner = null;
			Player p1 = null;
			Player p2 = null;
			if(DuelManager.getMap().size() >= 2){
				p1 = DuelManager.getMap().get(0)[0];
				p2 = DuelManager.getMap().get(0)[1];
				if(p1.equals(death) || p2.equals(death)){
					if(p1.equals(death)){
						winner = p2;
					}else{
						winner = p1;
					}
					DuelManager.finish(winner, death);
				}
			}
		}
	}
	
	@EventHandler
	public void rageQuitEvent(PlayerQuitEvent e){
		Player p = e.getPlayer();
		Player p1 = null;
		Player p2 = null;
		if(DuelManager.getMap().size() >= 2){
			p.chat("/spawn");
			
			p1 = DuelManager.getMap().get(0)[0];
			p2 = DuelManager.getMap().get(0)[1];
			
			if((p1.equals(p)) || (p2.equals(p))){
				Player winner;
				if(p1.equals(p)){
					winner = p2;
				}else{
					winner = p1;
				}
				DuelManager.finish(winner, p);
			}
		}
		
	}

}
