package fr.discowzombie.paragonduel;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import fr.discowzombie.paragonduel.command.DuelCmd;
import fr.discowzombie.paragonduel.events.DuelDeath;

public class ParagonDuel extends JavaPlugin {
	
	private static ParagonDuel instance;
	
	@Override
	public void onEnable() {
		instance = this;
		
		PluginManager pm = Bukkit.getServer().getPluginManager();
		pm.registerEvents(new DuelDeath(), this);
		
		getCommand("duel").setExecutor(new DuelCmd());
		
		super.onEnable();
	}
	
	@Override
	public void onDisable() {
		super.onDisable();
	}
	
	public static ParagonDuel getInstance(){
		return instance;
	}

}
