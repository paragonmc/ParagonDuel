package fr.discowzombie.paragonduel;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Statistic;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public abstract class DuelManager {
	
	private static ArrayList<Player[]> duel = new ArrayList<>();
	
	public static ArrayList<Player[]> getMap(){
		return duel;
	}
	
	public static boolean canDuel(){
		if(duel.size() == 0){
			return true;
		}
		return false;
	}
	
	static int timer = 6;
	static int starttask;
	
	public static void startDuel(Player p1, Player p2){
		if(canDuel()){
			Location loc1 = new Location(Bukkit.getWorld("spawn_skyblock"), -20, 149, 25);
			Location loc2 = new Location(Bukkit.getWorld("spawn_skyblock"), -5, 149, 24);
			p1.teleport(loc1);
			p2.teleport(loc2);
			Player[] pls = { p1, p2};
			DuelManager.getMap().add(pls);
	
			starttask = Bukkit.getScheduler().scheduleSyncRepeatingTask(ParagonDuel.getInstance(), new Runnable() {
				
				@Override
				public void run() {
					timer--;
					
					for(Player p : pls){
						p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 5*20, 250));
						//Ok
						p.sendMessage("§7[§6§lP§e§lDuel§7] » §bDébut du duel dans §9"+timer+" §b" + (timer > 1 ? "secondes" : "seconde") + "§b.");
					}
					
					if(timer == 1){
						Bukkit.getScheduler().cancelTask(starttask);
						timer = 6;
						for(Player p : pls){
							p.setGameMode(GameMode.ADVENTURE);
							p.setHealth(20);
							p.setFoodLevel(20);
							p.sendMessage("§7[§6§lP§e§lDuel§7] » §bLe duel commence, bonne chance !!");
						}
					}
				}
			}, 20, 20);
		}
	}
	
	public static void finish(Player winner, Player looser){
		winner.chat("/spawn");
		winner.sendMessage("§7[§6§lP§e§lDuel§7] » §bFélicitation, vous avez gagnez ce duel !");
		looser.sendMessage("§7[§6§lP§e§lDuel§7] » §bMalheuresement, vous avez perdu ce duel.");
		Bukkit.broadcastMessage("§7[§6§lP§e§lDuel§7] » §c"+winner.getName()+" §3{"+winner.getStatistic(Statistic.PLAYER_KILLS)+" kills} §7a tué §c"+looser.getName()+" §3{"+looser.getStatistic(Statistic.PLAYER_KILLS)+" kills}§7!");
		winner.setHealth(20);
		winner.setFoodLevel(20);
		winner.setGameMode(GameMode.SURVIVAL);
		duel.clear();
	}

}
