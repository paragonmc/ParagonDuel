package fr.discowzombie.paragonduel.command;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.discowzombie.paragonduel.DuelManager;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.ClickEvent.Action;

public class DuelCmd implements CommandExecutor {

	HashMap<Player, Player> propDuel = new HashMap<>();
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String str, String[] args) {
		if(sender instanceof Player){
			Player p = (Player) sender;
			if((args.length == 1) && (args[0].equalsIgnoreCase("accept"))){
				Player playerOther = propDuel.get(p);
				if((playerOther != null) && (playerOther.isOnGround())){
					//impossible de duel pour le moment
					if(DuelManager.canDuel()){
						DuelManager.startDuel(p, playerOther);
					}else{
						p.sendMessage("§7[§6§lP§e§lDuel§7] » §bUn duel est déjà en cours !");
						playerOther.sendMessage("§7[§6§lP§e§lDuel§7] » §bUn duel est déjà en cours !");
					}
					propDuel.remove(p);
				}
			}else if(args.length == 1){
				Player cible = Bukkit.getPlayer(args[0]);
				if((cible != null) && (cible.isOnline()) && (cible != p)){
					p.sendMessage("§7[§6§lP§e§lDuel§7] » §bVous avez envoyé une demande de duel à §9"+cible.getName()+" §b!");
					TextComponent tc = new TextComponent();
					tc.setText("§7[§6§lP§e§lDuel§7] » §bVous avez reçu une demande de duel de §9"+p.getName()+" §b! Pour accepter le duel: §a>> CLIQUER <<");
					tc.setClickEvent(new ClickEvent(Action.RUN_COMMAND, "/duel accept"));
					cible.spigot().sendMessage(tc);
					
					propDuel.put(cible, p);
				}else{
					p.sendMessage("§7[§6§lP§e§lDuel§7] » §cLe joueur cible est invalide !");
				}
			}else{
				p.sendMessage("§7[§6§lP§e§lDuel§7] » §cSyntaxe erreur : /duel <joueur>.");
			}
		}
		return true;
	}

}
